define ['marionette'], (Marionette) ->

  class AlbumsCollectionView extends Marionette.CollectionView
    tagName: 'div',
    className: 'albums-collection',

  AlbumsCollectionView
