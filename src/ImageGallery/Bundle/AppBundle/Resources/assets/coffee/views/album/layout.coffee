define ['marionette'], (Marionette) ->

  class AlbumLayoutView extends Marionette.Layout
    template: '#album-layout'
    regions:
      images: "#images"
      paginator: "#paginator"

  AlbumLayoutView
