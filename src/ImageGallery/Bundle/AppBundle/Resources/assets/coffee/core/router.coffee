define ['marionette'], (Marionette) ->

  class AppRouter extends Marionette.AppRouter
    appRoutes:
      "": "albums"

      "albums": "albums"
      "album/:id": "album"
      "album/:id/page/:page": "album"

      "*notFound": "404"

  AppRouter
