define [
  'marionette'
  'models/paginator'
  'models/album'
  'models/albumCollection'
  'views/albums/collection'
  'views/albums/item'
  'views/album/layout'
  'views/album/images'
  'views/album/paginator'
], (
  Marionette
  PaginatorModel
  AlbumModel
  AlbumCollectionModel
  AlbumsCollectionView
  AlbumsItemView
  AlbumLayoutView
  AlbumImagesView
  PaginatorView
) ->

  class AppController extends Marionette.Controller
    initialize:(opt)->
      @app = opt.app
    albums: ->
      albumCollection = new AlbumCollectionModel()
      albumCollection.fetch(success:=>
        @app.trigger('progress:hide')
      )

      view = new AlbumsCollectionView(
        collection: albumCollection
        itemView: AlbumsItemView
      )
      @app.mainRegion.show(view)

    album: (id, page)->
      paginator = new PaginatorModel(id: id)
      album = new AlbumModel(id: id, page: page ? 1)

      album.fetch(success: (model, response)=>
        paginator.set response.paginator
      )

      albumLayoutView = new AlbumLayoutView()
      @app.mainRegion.show(albumLayoutView)

      albumLayoutView.getRegion('images').show(new AlbumImagesView(model: album));
      albumLayoutView.getRegion('paginator').show(new PaginatorView(model: paginator));

    404: ->
      Backbone.history.navigate('', trigger: on)

  AppController
