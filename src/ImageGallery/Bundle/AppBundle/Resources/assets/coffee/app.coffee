define [
  'config'
  'backbone'
  'marionette'
  'core/router'
  'core/controller'
], (
  Config
  Backbone
  Marionette
  AppRouter
  AppController
) ->

  app = new Marionette.Application

  app.addRegions
    mainRegion: '#main-container'

  app.on 'start', ->
    app.router = new AppRouter(
      controller: new AppController(app:app)
    )

    if not Backbone.history.started
      Backbone.history.start
        pushState: true
        root: Config.baseUrl

  app