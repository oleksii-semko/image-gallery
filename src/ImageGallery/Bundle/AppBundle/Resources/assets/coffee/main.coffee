requirejs.config
  paths:
    backbone: 'vendor/backbone'
    underscore: 'vendor/underscore'
    jquery: 'vendor/jquery'
    marionette: 'vendor/marionette'
    'backbone.babysitter': 'vendor/babysitter'
    'backbone.wreqr': 'vendor/wreqr'

require [
  'app'
], (App) ->

  App.start()