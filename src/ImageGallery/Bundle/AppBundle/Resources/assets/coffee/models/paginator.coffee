define ['backbone'], (Backbone) ->

  class Paginator extends Backbone.Model
    defaults:
       currentPageNum: 0,
       numItemsPerPage: 0,
       totalCount: 0,
       pageCount: 0

  Paginator