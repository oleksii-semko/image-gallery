define ['backbone'], (Backbone) ->

  class Album extends Backbone.Model
    urlRoot: '/api/v1/album/'
    url: ->
      @urlRoot + @id + '/page/' + @get('page')
    defaults:
      id: 0
      name: ''
      images: []
    parse: (response)->
      if response.album? then response.album else response

  Album