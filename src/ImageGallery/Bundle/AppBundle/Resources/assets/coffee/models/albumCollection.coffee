define [
  'backbone'
  'models/album'
], (
  Backbone
  Album
) ->

  class AlbumCollection extends Backbone.Collection
    model: Album
    url: '/api/v1/albums'

  AlbumCollection
