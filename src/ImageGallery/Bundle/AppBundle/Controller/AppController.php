<?php

namespace ImageGallery\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AppController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImageGalleryAppBundle:App:index.html.twig');
    }
}
