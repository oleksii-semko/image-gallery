<?php

namespace ImageGallery\Bundle\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteResource("Album", pluralize=false)
 */
class ImageController extends FOSRestController
{
    /**
     * @ApiDoc(
     *     description="Get album images for the 1st page.",
     *     statusCodes={
     *         200="On Success"
     *     }
     * )
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction($id)
    {
        $api_service = $this->container->get('api_service');
        $serializer = $this->get('jms_serializer');
        $images = $api_service->getPaginatedImages($id);

        return new Response(
            $serializer->serialize($images, 'json'),
            Response::HTTP_OK,
            ['Content-Type' => 'application/json']
        );
    }

    /**
     * @ApiDoc(
     *     description="Get album images per page. (Pagination)",
     *     statusCodes={
     *         200="On Success"
     *     }
     * )
     *
     * @param $id
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPageAction($id, $page)
    {
        $api_service = $this->container->get('api_service');
        $serializer = $this->get('jms_serializer');
        $images = $api_service->getPaginatedImages($id, $page);

        return new Response(
            $serializer->serialize($images, 'json'),
            Response::HTTP_OK,
            ['Content-Type' => 'application/json']
        );

    }
}
