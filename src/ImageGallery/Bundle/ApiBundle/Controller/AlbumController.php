<?php

namespace ImageGallery\Bundle\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

class AlbumController extends FOSRestController
{
    /**
     * @ApiDoc(
     *     description="Get all albums.",
     *     statusCodes={
     *         200="On Success"
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAlbumsAction()
    {
        $api_service = $this->container->get('api_service');
        $albums = $api_service->getAlbums();
        $serializer = $this->get('jms_serializer');

        return new Response(
            $serializer->serialize($albums, 'json'),
            Response::HTTP_OK,
            ['Content-Type' => 'application/json']
        );
    }
}
