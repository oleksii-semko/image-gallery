<?php

namespace ImageGallery\Bundle\ApiBundle\Datafikstures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ImageGallery\Bundle\ApiBundle\Entity\Album;
use ImageGallery\Bundle\ApiBundle\Entity\Image;

class ImageFixtures extends AbstractFixture implements  OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 88; $i++) {

            if ($i < 6)             $album = 'album-1';
            if ($i > 5 && $i < 26)  $album = 'album-2';
            if ($i > 25 && $i < 46) $album = 'album-3';
            if ($i > 45 && $i < 66) $album = 'album-4';
            if ($i > 65)            $album = 'album-5';

            $image = new Image();
            $image->setTitle('Pic. ' . $i);
            $image->setAlbum($manager->merge($this->getReference($album)));
            $image->setAlt("Pic. $i alternative text");
            $image->setFilename('image' . $i % 2 . '.jpg');

            $manager->persist($image);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}
