<?php

namespace ImageGallery\Bundle\ApiBundle\Datafikstures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ImageGallery\Bundle\ApiBundle\Entity\Album;

class AlbumFixtures extends AbstractFixture implements OrderedFixtureInterface
{


    public function load(ObjectManager $manager)
    {
        $description = [
            'Lorem ipsum dolor sit amet.',
            'Morbi ut velocity magna.',
            'Etiam vehicula nunc non leo hendrerit commodo.',
            'Vestibulum vulputate mauris eget erat.',
            'Nulla consectetur tempus nisl vitae viverra.',
            'Cras el mauris eget erat congue dapibus.',
            'Nulla consectetur tempus nisl vitae viverra.',
            'Cras elementum molestie vestibulum.',
            'Morbi id quam nisl.',
        ];

        for ($i = 1; $i <= 5; $i++) {
            shuffle($description);

            ${'album' . $i} = new Album();
            ${'album' . $i}->setTitle($description[0]);
            ${'album' . $i}->setDescription(implode(" ", $description));
            $manager->persist(${'album' . $i});
        }

        $manager->flush();

        for ($i = 1; $i <= 5; $i++) {
            $this->addReference('album-' . $i, ${'album' . $i});
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}
