<?php

namespace ImageGallery\Bundle\ApiBundle\Services;

use ImageGallery\Bundle\ApiBundle\Repository\ImageRepository;
use ImageGallery\Bundle\ApiBundle\Repository\AlbumRepository;
use Doctrine\ORM\EntityManager;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;

class ApiService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Paginator
     */
    private $paginator;

    public function __construct(
        EntityManager $entityManager,
        Paginator $paginator
    )
    {
        $this->em = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @param string $name
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($name)
    {
        return $this->em->getRepository($name);
    }

    /**
     * @return array
     */
    public function getAlbums()
    {
        /** @var $repo AlbumRepository */
        $repo = $this->getRepository("ImageGalleryApiBundle:Album");

        $result = $repo->fetchAlbums();

        return $this->collectAlbums($result);
    }

    /**
     * @param $albumId
     * @param int $page
     * @return array
     */
    public function getPaginatedImages($albumId, $page = 1)
    {
        /** @var $repo ImageRepository */
        $repo = $this->getRepository("ImageGalleryApiBundle:Image");
        $query = $repo->fetchImagesQuery($albumId);

        /** @var $pagination SlidingPagination */
        $pagination = $this->paginator->paginate($query, $page);

        return [
            'paginator' => [
                'currentPageNum' => (int)$pagination->getCurrentPageNumber(),
                'pageCount' => (int)$pagination->getPageCount(),
            ],
            'images' => $pagination->getItems(),
        ];
    }

    /**
     * @param $albums
     * @return array
     */
    public function collectAlbums($albums) {
        $sorted = $result = [];

        foreach ($albums as $v) {
            if (!array_key_exists($v['id'], $sorted)) {
                $sorted[$v['id']] = [
                    'id' => $v['id'],
                    'title' => $v['title'],
                    'description' => $v['description'],
                    'created' => $v['created'],
                    'images' => [],
                ];
            }

            $sorted[$v['id']]['images'][] = [
                'id' => $v['img_id'],
                'title' => $v['img_title'],
                'filename' => $v['filename'],
                'alt' => $v['img_alt'],
            ];
        }

        foreach ($sorted as $v) {
            $result[] = $v;
        }

        return $result;
    }
}