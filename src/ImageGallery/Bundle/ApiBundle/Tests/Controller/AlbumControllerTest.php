<?php

namespace ImageGallery\Bundle\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumControllerTest extends WebTestCase
{
    public function testGetAlbums()
    {
        $client = static::createClient();
        $request = $client->request('GET', '/api/v1/albums');

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            "Unexpected HTTP status code for GET /api/v1/albums"
        );

        $response = $request->getResponse();

        $this->assertEquals(
            5,
            count($response),
            "Unexpected number of albums"
        );

        foreach ($response as $album) {
            $this->assertLessThan (
                11,
                count($album->images),
                "Unexpected number of images in album #id:" . $album->id
            );
        }
    }
}
