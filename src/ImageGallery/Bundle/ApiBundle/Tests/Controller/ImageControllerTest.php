<?php

namespace ImageGallery\Bundle\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ImageControllerTest extends WebTestCase
{
    public function testGetAction()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/album/1');

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            "Unexpected HTTP status code for GET /api/v1/album/1"
        );

        $response = $client->getResponse();

        $this->assertNotEmpty($response, 'No images in response');
        $this->assertNotEmpty($response['paginator'], 'No paginator in response');
        $this->assertNotEmpty($response['images'], 'No images in response');

    }

    public function testGetPageAction()
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/album/1/page/1');

        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            "Unexpected HTTP status code for GET /api/v1/album/1/page/1"
        );

        $response = $client->getResponse();

        $this->assertNotEmpty($response, 'No images in response');
        $this->assertNotEmpty($response['paginator'], 'No paginator in response');
        $this->assertNotEmpty($response['images'], 'No images in response');

        $this->assertLessThan(
            11,
            count($response['images']),
            "Unexpected number of images per page"
        );
    }
}
