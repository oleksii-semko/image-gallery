#!/usr/bin/env bash

composer install
npm install
bower install
php app/console doctrine:database:create --no-interaction
php app/console doctrine:schema:update --force --no-interaction
php app/console doctrine:fixtures:load --no-interaction
grunt
php app/console server:run
