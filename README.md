### Requirements

- [Composer](https://getcomposer.org/download)
- [NPM](https://www.npmjs.org)
- [Bower](http://bower.io)
- [Grunt](http://gruntjs.com)

### Installation steps

```bash
$ git clone git@bitbucket.org:oleksii-semko/image-gallery.git
$ cd <project_dir>
$ sh build.sh
```
