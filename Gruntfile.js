module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        publicDir: 'web/bundles/gallery',
        baseAssetsDir: 'src/ImageGallery/Bundle/AppBundle/Resources/assets',
        coffeeDir: '<%= baseAssetsDir %>/coffee',
        sassDir: '<%= baseAssetsDir %>/scss',
        picsResDir: '<%= baseAssetsDir %>/images',
        cssDir: '<%= publicDir %>/css',
        picsDestDir: '<%= publicDir %>/images',
        jsDir: '<%= publicDir %>/js',
        vendorsDir: '<%= baseAssetsDir %>/vendor',

        // Cleans the public assets
        clean: {
            build: {
                src: ['<%= publicDir %>/**']
            }
        },

        // Copies files
        copy: {
            pics: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= picsResDir %>',
                        src: ['**/*.jpg'],
                        dest: '<%= picsDestDir %>'
                    }
                ]
            },
            js: {
                files: [
                    {
                        src: ['<%= vendorsDir %>/jquery/dist/jquery.js'],
                        dest: '<%= jsDir %>/vendor/jquery.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/backbone/backbone.js'],
                        dest: '<%= jsDir %>/vendor/backbone.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/backbone.babysitter/lib/backbone.babysitter.js'],
                        dest: '<%= jsDir %>/vendor/babysitter.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/backbone.marionette/lib/core/amd/backbone.marionette.js'],
                        dest: '<%= jsDir %>/vendor/marionette.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/backbone.wreqr/lib/backbone.wreqr.js'],
                        dest: '<%= jsDir %>/vendor/wreqr.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/requirejs/require.js'],
                        dest: '<%= jsDir %>/vendor/require.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/underscore/underscore.js'],
                        dest: '<%= jsDir %>/vendor/underscore.js'
                    },
                    {
                        src: ['<%= vendorsDir %>/backbone-query-parameters/backbone.queryparams.js'],
                        dest: '<%= jsDir %>/vendor/backbone.queryparams.js'
                    }
                ]
            }
        },

        // Compiles the sass files into the public js
        compass: {
            dev: {
                options: {
                    sassDir: '<%= sassDir %>',
                    cssDir: '<%= cssDir %>',
                    environment: 'development'
                }
            },
            prod: {
                options: {
                    sassDir: '<%= sassDir %>',
                    cssDir: '<%= cssDir %>',
                    environment: 'production',
                    outputStyle: 'compressed'
                }
            }
        },

        // Compiles coffee files
        coffee: {
            main: {
                expand: true,
                cwd: '<%= coffeeDir %>',
                src: ['**/*.coffee'],
                dest: '<%= jsDir %>',
                ext: '.js'
            }
        },

        // Lints the coffee files
        coffeelint: {
            main: {
                files: [
                    {
                        expand: true,
                        src: ['<%= coffeeDir %>/**/*.coffee']
                    }
                ]
            }
        },

        // Uglifies compiled js files
        uglify: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= jsDir %>',
                        src: '**/*.js',
                        dest: '<%= jsDir %>'
                    }
                ]
            }
        },

        watch: {
            sass: {
                files: ['<%= sassDir %>/**'],
                tasks: ['sass:watch'],
                options: {
                    spawn: false
                }
            },
            coffee: {
                files: ['<%= coffeeDir %>/**'],
                tasks: ['coffee:watch'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-coffeelint');

    // Task for building SASS file changes
    grunt.registerTask('sass:build', ['compass:dev']);

    // Task for building Coffee file changes
    grunt.registerTask('coffee:build', ['coffee:main']);

    // Task to run when deploying
    grunt.registerTask('dev', ['clean', 'copy:pics', 'copy:js', 'compass:prod', 'coffee']);

    // Task to run when deploying
    grunt.registerTask('prod', ['clean', 'copy:pics', 'copy:js', 'compass:prod', 'coffee', 'uglify']);

    // Default task
    grunt.registerTask('default', ['prod']);
};